package adecWatt.view.jProp;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import misc.ScaledImage;
import misc.Util;

import adecWatt.model.ImageDB;
import adecWatt.view.JEditable;
import adecWatt.view.JProp;
import static adecWatt.model.Prop.PropMix;

public abstract class JPImage extends JProp {
    // ========================================
    protected JLabel jImage = new JLabel ();
    protected ScaledImage scaledImage;
    protected ImageDB rightDB;

    public JPImage (String propName, JEditable jEditable) {
	super (propName, jEditable);
	jImage.setHorizontalAlignment (SwingConstants.CENTER);
    }

    protected void updateView () {
	setImage ();
	Util.packWindow (jImage);
    }

    protected abstract ImageIcon getFitImage ();
    protected void setImage () {
	scaledImage = null;
	if (lastValue == null) {
	    jImage.setIcon (Util.loadActionIcon ("EmptyIcon"));
	    return;
	}
	if (lastValue == PropMix) {
	    jImage.setIcon (Util.loadActionIcon ("MixIcon"));
	    return;
	}
	scaledImage = rightDB.getImage (lastValue, horizontalSpin);
	if (scaledImage == null) {
	    jImage.setIcon (Util.loadActionIcon ("BreakIcon"));
	    return;
	}
	jImage.setIcon (getFitImage ());
    }

    protected void setPopup () {
	jImage.addMouseListener (new MouseAdapter () {
		public void mousePressed (MouseEvent e) {
		    System.err.flush ();
		    if (!SwingUtilities.isRightMouseButton (e))
			return;
		    new JImagePopup (jImage, e.getPoint (), JPImage.this, rightDB);
		}
	    });
    }

    // ========================================
}
