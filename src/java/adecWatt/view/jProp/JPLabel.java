package adecWatt.view.jProp;

import java.util.HashSet;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.text.JTextComponent;

import misc.Util;
import static misc.Util.GBC;
import static misc.Util.GBCLNL;

import adecWatt.view.JEditable;
import adecWatt.view.JProp;
import static adecWatt.model.Prop.PropMix;

public abstract class JPLabel extends JProp {

    // ========================================
    protected JTextComponent[] editors;
    protected HashSet<JTextComponent> unchanged = new HashSet<JTextComponent> ();

    protected boolean verify (JTextComponent editor) {
	return true;
    }
    
    public void setUnchange (JTextComponent editor, boolean unchange) {
	super.setUnchange (editor, unchange);
	if (unchange)
	    unchanged.add (editor);
	else
	    unchanged.remove (editor);
	verify (editor);
    }

    // ========================================
    public JPLabel (String propName, JEditable jEditable) {
	super (propName, jEditable);
    }

    protected abstract JComponent getEditor ();
    protected JComponent getValue () {
	if (lastValue == null)
	    return null;
	if (lastValue == PropMix)
	    return getUnknownLabel ();
	return new JLabel (lastValue);
    }

    protected void displayByType (boolean edit, JPanel jPropList, JTabbedPane jTabbedPane) {
	JComponent jValue = edit ? getEditor () : getValue ();
	if (jValue == null)
	    return;
	Util.addComponent (localizedUserLabel.getJLabel (), jPropList, GBC);
	Util.addComponent (new JLabel (" : "), jPropList, GBC);
	Util.addComponent (jValue, jPropList, GBCLNL);
    }

    // ========================================
    public String getLastValue () {
	String val = editors[0].getText ();
	if (unchanged.size () > 0)
	    val = PropMix;
	return val;
    }

    // ========================================
}
