package adecWatt.view.jProp;

import javax.swing.JComponent;
import javax.swing.JTextField;

import adecWatt.view.JEditable;
import static adecWatt.model.Prop.PropMix;

public class JPropText extends JPLabel {

    // ========================================
    public JPropText (String propName, JEditable jEditable) {
	super (propName, jEditable);
    }

    protected JComponent getEditor () {
	JTextField editor = new JTextField (lastValue, 12);
	editors = new JTextField [] {editor};
	setUnchange (editor, lastValue == PropMix);
	setChangeListener (editor);
	return editor;
    }

    // ========================================
}
