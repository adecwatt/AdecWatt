package adecWatt.view.jProp;

import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import misc.ScaledImage;
import misc.Util;

import adecWatt.view.JEditable;
import adecWatt.view.JProp;

public class JPropPreview extends JProp {
    // ========================================
    protected JLabel jImage = new JLabel ();
    protected ScaledImage scaledImage;

    public JPropPreview (String propName, JEditable jEditable) {
	super (propName, jEditable);
	jImage.setHorizontalAlignment (SwingConstants.CENTER);
	jImage.addComponentListener (new ComponentAdapter () {
		public void componentResized (ComponentEvent e) {
		    if (scaledImage == null)
			return;
		    Rectangle bounds = jImage.getBounds ();
		    jImage.setIcon (scaledImage.getInsideFit (bounds.width, bounds.height, ScaledImage.defaultAdjust));
		}
	    });
   }

    protected void updateView () {
	setImage ();
	Util.packWindow (jImage);
    }

    protected void setImage () {
	scaledImage = jEditable.getPreview ();
	if (scaledImage == null) {
	    jImage.setIcon (Util.loadActionIcon ("BreakIcon"));
	    return;
	}
	jImage.setIcon (scaledImage.getInsideFit (defaultImageWidth, defaultImageHeight, ScaledImage.defaultAdjust));

	// try {
	//     javax.imageio.ImageIO.write (scaledImage.original, "png", new java.io.File ("/tmp/toto.png"));
	// } catch (Exception e) {
	// }
    }

    protected void displayByType (boolean edit, JPanel jPropList, JTabbedPane jTabbedPane) {
	setImage ();
	if (scaledImage == null)
	    return;
	jTabbedPane.add (localizedUserLabel.getCurrentLabel (), jImage);
	jEditable.addLocalizedUserLabel (localizedUserLabel);
    }

    // ========================================
}
