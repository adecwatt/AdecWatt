package adecWatt.view.jProp;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.openstreetmap.gui.jmapviewer.DefaultMapController;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.interfaces.ICoordinate;
import org.openstreetmap.gui.jmapviewer.interfaces.TileSource;
import org.openstreetmap.gui.jmapviewer.tilesources.BingAerialTileSource;
import org.openstreetmap.gui.jmapviewer.tilesources.MapQuestOsmTileSource;
import org.openstreetmap.gui.jmapviewer.tilesources.OsmTileSource;

import misc.Util;

import adecWatt.model.Prop;
import adecWatt.view.JEditable;
import adecWatt.view.JProp;

public class JPropGeo extends JPMultiValues {

    // ========================================
    protected JMapViewer treeMap = new JMapViewer ();
    protected MapMarkerDot here;

    protected NumberFormat getNumberFormat () { return Prop.geoFormat; }

    protected boolean verify (JTextField editor) {
	boolean result = super.verify (editor);
	if (!result)
	    return false;
	updateGeo ();
	return true;
    }
    protected void updateGeo () {
	try {
	    here.setLat (Float.parseFloat (editors [0].getText ().replace (",", ".")));
	    here.setLon (Float.parseFloat (editors [1].getText ().replace (",", ".")));
	    treeMap.setDisplayToFitMapMarkers ();
	    treeMap.setZoom (16);
	} catch (Exception e) {
	}
    }
    public void setGeo (double latitude, double longitude) {
	if (!editors[0].getText ().isEmpty () && !editors[1].getText ().isEmpty ())
	    return;
	forceGeo (latitude, longitude);
	updateGeo ();
	jEditable.toFront (map);
    }
    protected void forceGeo (double latitude, double longitude) {
	editors[0].setText (Prop.geoFormat.format (latitude));
	editors[1].setText (Prop.geoFormat.format (longitude));
	here.setLat (latitude);
	here.setLon (longitude);
	treeMap.repaint ();
    }
    protected void setGeo () {
	try {
	    here.setLat (lastValues[0]);
	    here.setLon (lastValues[1]);
	    treeMap.setDisplayToFitMapMarkers ();
	    treeMap.setZoom (16);
	} catch (Exception e) {
	}
    }
    public JPropGeo (String propName, JEditable jEditable) {
	super (propName, jEditable);
	jEditable.setFirstGeo (this);
	treeMap.setScrollWrapEnabled (true);
	here = new MapMarkerDot (47.95707, -2.56396);
        treeMap.addMapMarker (here);
    }

    JPanel map;
    protected void displayByType (boolean edit, JPanel jPropList, JTabbedPane jTabbedPane) {
	if (!edit && (hidden || lastValue == null))
	    return;
	super.displayByType (edit, jPropList, jTabbedPane);
	setGeo ();
        JComboBox<TileSource> tileSourceSelector = new JComboBox<> (new TileSource[] {
                new OsmTileSource.Mapnik (),
                new OsmTileSource.CycleMap (),
                new BingAerialTileSource (),
                new MapQuestOsmTileSource ()
                //new MapQuestOpenAerialTileSource ()
	    });
        tileSourceSelector.addItemListener (new ItemListener () {
		public void itemStateChanged (ItemEvent e) {
		    treeMap.setTileSource ((TileSource) e.getItem ());
		}
	    });
	map = new JPanel (new BorderLayout ());
	map.add (Util.newLabel ("HelpOSM", SwingConstants.CENTER), BorderLayout.PAGE_START);
	map.add (treeMap, BorderLayout.CENTER);
	map.add (tileSourceSelector, BorderLayout.PAGE_END);
	jTabbedPane.add (localizedUserLabel.getCurrentLabel (), map);
	jEditable.addLocalizedUserLabel (localizedUserLabel);
	if (edit)
	    treeMap.addMouseListener (new DefaultMapController (treeMap) {
		    @Override
		    public void mouseClicked (MouseEvent e) {
			ICoordinate pos = map.getPosition (e.getPoint());
			forceGeo (pos.getLat (), pos.getLon ());
		    }
		});
    }

    // ========================================
}
