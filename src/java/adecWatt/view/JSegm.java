package adecWatt.view;

import adecWatt.model.Item;
import adecWatt.model.Segm;

@SuppressWarnings ("serial") public class JSegm extends JItem {

    // ========================================
    public Segm segm;

    public Item	getItem ()	{ return segm; }

    // ========================================
    public JSegm (JWorkspaceView<?> jWorkspaceView, Segm segm) {
	super (jWorkspaceView, segm);
    }

    public void setItem (Item item) {
	if (item == null)
	    return;
	segm = (Segm) item;
	setCurrentThetaDegree (item.getAngle ());
	setCurrentSize ();
	setCurrentPos ();
    }

    // ========================================
}
