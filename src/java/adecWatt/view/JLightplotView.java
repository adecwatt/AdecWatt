package adecWatt.view;

import adecWatt.model.Unit;
import adecWatt.model.unit.Building;
import adecWatt.model.unit.Lightplot;

@SuppressWarnings ("serial") public class JLightplotView extends JWorkspaceView<Lightplot> {

    protected Building building;

    // ========================================
    public JLightplotView (JAdecWatt jAdecWatt, Lightplot lightplot) {
	super (jAdecWatt, lightplot);
	changeBuilding ();
	workspace.stateNotifier.addUpdateObserver (this, Unit.BroadcastChangeBuilding);
	// XXX updateConnectionLayer ();
    }

    void changeBuilding () {
	if (building != null)
	    building.stateNotifier.removeObserver (this);
	building = workspace.getBuilding ();
	// XXX repaint
	building.stateNotifier.addMsgObserver (this,
					       Unit.BroadcastNewItem, Unit.BroadcastRemoveItem,
					       Unit.BroadcastChangePos, Unit.BroadcastChangeRot);
    }

    // ========================================
    public void updateChangeBuilding () {
	changeBuilding ();
	realSize = workspace.getRealSize ();
	//scaledBlueprint = workspace.getBlueprint ();
	//setBlueprint ();
	setScale (scale);
    }

    // ========================================
}
