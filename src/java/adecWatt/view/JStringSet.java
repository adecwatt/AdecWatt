package adecWatt.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Collection;
import java.util.Collections;
import java.util.Vector;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import misc.Util;

@SuppressWarnings ("serial")
public class JStringSet extends JPanel {

    // ========================================
    static public final String
	actionAdd	= "Add",
	actionRemove	= "Remove";

    // ========================================
    private Vector<String> refList = new Vector<String> ();
    private Vector<String> list = new Vector<String> ();
    private JList<String> jList = new JList<String> ();
    private JTextField jTextField = new JTextField (8);

    public Collection<String> getSet () {
	if (list.size () < 1)
	    return null;
	return list;
    }

    // ========================================
    public JStringSet (Collection<String> enumChoice) {
	super (new BorderLayout ());
	jTextField.addAncestorListener (new AncestorListener () {
		@Override
		public void ancestorRemoved (final AncestorEvent event) {}
		@Override
		public void ancestorMoved (final  AncestorEvent event) {}
		@Override
		public void ancestorAdded (final AncestorEvent event) {
		    jTextField.requestFocusInWindow ();
		}
	    });
	jTextField.addFocusListener (new FocusListener () {
		@Override
		public void focusGained (final FocusEvent e) {}
		@Override
		public void focusLost (final FocusEvent e) {
		    if (isFirstTime) {
			// When we lose focus, ask for it back but only once
			jTextField.requestFocusInWindow ();
			isFirstTime = false;
		    }
		}
		private boolean isFirstTime = true;
	    });
 	jList.addListSelectionListener (new ListSelectionListener () {
	    public void valueChanged (ListSelectionEvent e) {
		jTextField.setText (jList.getSelectedValue ());
	    }
	    });

	JPanel jPanel = new JPanel (new BorderLayout ());
	jPanel.add (Util.newIconButton (actionAdd, new ActionListener () {
		public void  actionPerformed (ActionEvent e) {
		    actionAdd ();
		}
	    }), BorderLayout.WEST);
	jPanel.add (jTextField, BorderLayout.CENTER);
	jPanel.add (Util.newIconButton (actionRemove, new ActionListener () {
		public void  actionPerformed (ActionEvent e) {
		    actionRemove ();
		}
	    }), BorderLayout.EAST);
	add (jPanel, BorderLayout.SOUTH);
	add (Util.getJScrollPane (jList), BorderLayout.CENTER);
	Util.unBoxButton (jPanel);
	if (enumChoice == null)
	    return;
	refList.addAll (enumChoice);
	Collections.sort (refList);
	list.addAll (refList);
    }

    // ========================================
    public void init () {
	list.clear ();
	list.addAll (refList);
	updateList ();
    }
    public void confirm () {
	actionAdd ();
	refList.clear ();
	refList.addAll (list);
    }

    // ========================================
    public void updateList () {
	jList.setListData (list);
	jList.revalidate ();
    }

    public void actionAdd () {
	String value = jTextField.getText ();
	jTextField.setText ("");
	if (value == null || value.isEmpty ())
	    return;
	if (list.contains (value))
	    return;
	list.addElement (value);
	Collections.sort (list);
	updateList ();
	// XXX jList.setSelectedValue (value, true);
	jList.setSelectedValue (null, false);
    }
    public void actionRemove () {
	int selection = jList.getSelectedIndex ();
	if (selection < 0)
	    return;
	list.removeElementAt (selection);
	updateList ();
	if (selection >= list.size ())
	    selection--;
	jList.setSelectedIndex (selection);
    }
    // ========================================
}
