package adecWatt.view;

import adecWatt.model.Acc;
import adecWatt.model.Item;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import javax.swing.JLabel;

@SuppressWarnings ("serial")
public class JAcc extends JLabel {

    // ========================================
    protected JWorkspaceView<?> jWorkspaceView;
    private Item item;
    private Acc acc;

    public Acc	getAcc ()	{ return acc; }
    public Item	getItem ()	{ return item; }

    // ========================================
    public JAcc (JWorkspaceView<?> jWorkspaceView, Item item, Acc acc) {
	this.jWorkspaceView = jWorkspaceView;
	this.item = item;
	this.acc = acc;
	setIcon (acc.getModel ().getIcon (16));
	setSize (getPreferredSize ());
	setPos ();
	// AccMouseAdapter accMouseAdapter = new AccMouseAdapter ();
	// addMouseListener (accMouseAdapter);
	// addMouseMotionListener (accMouseAdapter);
    }
    public void setPos () {
	setPos (jWorkspaceView.scaleModelToView (item.getAccCenter (acc.getId ())));
    }
    public void setPos (Point pos) {
	pos.x -= getWidth ()/2;
	pos.y -= getHeight ()/2;
	setLocation (pos);
    }

    public void setScale () {
    // 	center = new double [] {accPos.x, accPos.y};
    // 	jComp.getAT ().transform (center, 0, center, 0, 1);
    // 	setLocation ((int) (center[0]+(jComp.getWidth ()-getWidth ())/2), (int) (center[1]+(jComp.getHeight ()-getHeight ())/2));
    // 	String accConnectedTo = acc.getConnectedTo ();
    // 	if (accConnectedTo != null)
    // 	    jComp.addConnectedAcc (acc);
    }

    // ========================================
    public class AccMouseAdapter extends MouseAdapter {
	public void mousePressed (MouseEvent e) {
	    jWorkspaceView.mousePressed (e);
	}
	public void mouseReleased (MouseEvent e) {
	    jWorkspaceView.mouseReleased (e);
	}
	public void mouseDragged (MouseEvent e) {
	    jWorkspaceView.mouseDragged (e);
	}
    }

    // ========================================
    // JDragUnit selectedJDragAcc;
    // Point mousePosOnJDragUnit;

    // public void startDrag (MouseEvent e) {
    // 	JRootPane jRootPane = getRootPane ();
    // 	JLayeredPane jLayeredPane = jRootPane.getLayeredPane ();
    // 	if (selectedJDragAcc != null)
    // 	    jLayeredPane.remove (selectedJDragAcc);
    // 	selectedJDragAcc = new JDragUnit (acc.getModel ());
    // 	mousePosOnJDragUnit = e.getPoint ();
    // 	Point absPos = jRootPane.getMousePosition (true);
    // 	selectedJDragAcc.setLocation (absPos.x-mousePosOnJDragUnit.x, absPos.y-mousePosOnJDragUnit.y);
    // 	jLayeredPane.add (selectedJDragAcc);
    // 	jLayeredPane.setLayer (selectedJDragAcc, JLayeredPane.DRAG_LAYER.intValue ());
    // 	//repaint ();
    // }

    // public void stopDrag (MouseEvent e) {
    // 	if (selectedJDragAcc == null)
    // 	    return;
    // 	selectedJDragAcc.setVisible (false);
    // 	selectedJDragAcc.getParent ().remove (selectedJDragAcc);
    // 	// XXX recherche comp et acc
    // 	try {
    // 	    AdecWatt adecWatt = jWorkspaceView.getJAdecWatt ().getAdecWatt ();
    // 	    // XXX Il peut y en avoir plusieurs
    // 	    ArrayList<Comp> dstComps = jWorkspaceView.findComps (SwingUtilities.convertPoint (this, e.getPoint (), jWorkspaceView));
    // 	    if (dstComps.size () < 1) {
    // 		adecWatt.broadcastDisplay (AdecWattManager.actionRemoveAcc, jWorkspaceView, acc);
    // 		return;
    // 	    }
    // 	    // XXX
    // 	    // PermanentDB permanentDB = adecWatt.getPermanentDB ();
    // 	    // JAcc dstJAcc = dstJComp.getJAcc (SwingUtilities.convertPoint (this, e.getPoint (), dstJComp));
    // 	    // if (acc.getDirectUnit ().isDescendingFrom (permanentDB.getPowerPlugId ()))
    // 	    // 	jWorkspaceView.storyConnect (jComp, this, dstJComp, dstJAcc, permanentDB.getPowerSocketId ());
    // 	} catch (Exception e2) {
    // 	    e2.printStackTrace ();
    // 	}
    // }
    // public void drag (MouseEvent e) {
    // 	if (selectedJDragAcc == null)
    // 	    return;
    // 	Point absPos = getRootPane ().getMousePosition (true);
    // 	try {
    // 	    selectedJDragAcc.setLocation (absPos.x-mousePosOnJDragUnit.x, absPos.y-mousePosOnJDragUnit.y);
    // 	} catch (Exception e2) {
    // 	}
    // }
    // ========================================
    public boolean contains (Point2D.Double realPos, double close) {
	double theta = Math.toRadians (-item.getThetaDegree ());
	return item.onAcc (acc.getId (), theta, realPos, close);
    }

    // ========================================
    public void dragAcc (Dimension delta) {
	Point oldUnitPos = getLocation ();
	setLocation (oldUnitPos.x+delta.width, oldUnitPos.y+delta.height);
    }

    // ========================================
}
