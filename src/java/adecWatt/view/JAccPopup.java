package adecWatt.view;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JPopupMenu;

import misc.Util;

import adecWatt.control.AdecWattManager;
import adecWatt.model.Acc;
import adecWatt.model.Item;
import adecWatt.model.AdecWatt;
import adecWatt.model.User;

@SuppressWarnings ("serial") public class JAccPopup extends JPopupMenu {

    private Acc getLocal (JWorkspaceView<?> jWorkspaceView, Item item, Acc acc) {
	// return acc;
	item = jWorkspaceView.workspace.getCloseEmbedded (item);
	return (Acc) item.getCloseEmbedded (acc);
    }
    private List<Acc> getLocal2 (JWorkspaceView<?> jWorkspaceView, Item item, Acc acc) {
	ArrayList<Acc> localSelectedAccs = new ArrayList<Acc> (1);
	localSelectedAccs.add (jWorkspaceView.workspace.getCloseEmbedded (item).getCloseEmbedded (acc));
	return localSelectedAccs;
    }

    // ========================================
    public JAccPopup (final JWorkspaceView<?> jWorkspaceView, final Item item, final Acc acc, Point pos) {
	final AdecWatt adecWatt = jWorkspaceView.getAdecWatt ();
	//Item localItem = jWorkspaceView.workspace.getLocalEmbedded (item.getId ());
	User user = adecWatt.getUser ();
	boolean editor = user.isEditor (jWorkspaceView.getWorkspace ());
	final List<Acc> selectedAccs = Arrays.asList (acc);

	Util.addMenuItem (AdecWattManager.actionDisplayAcc, new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    adecWatt.broadcastDisplay (AdecWattManager.actionDisplayAcc, selectedAccs);
		}
	    }, this);
	if (editor) {
	    Util.addMenuItem (AdecWattManager.actionModifyAcc, new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			adecWatt.broadcastDisplay (AdecWattManager.actionModifyAcc, getLocal2 (jWorkspaceView, item, acc));
		    }
		}, this);
	    Util.addMenuItem (AdecWattManager.actionTransformAcc, new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			adecWatt.broadcastDisplay (AdecWattManager.actionTransformAcc, getLocal (jWorkspaceView, item, acc));
		    }
		}, this);
	    // if (item != null && item.getLocalEmbedded (acc.getId ()) != null)
	    //     Util.addMenuItem (AdecWattManager.actionRemoveAcc, new ActionListener () {
	    // 	    public void actionPerformed (ActionEvent e) {
	    // 		adecWatt.broadcastDisplay (AdecWattManager.actionRemoveAcc, jWorkspaceView, acc);
	    // 	    }
	    // 	}, this);
	}
	show (jWorkspaceView, pos.x, pos.y);
    }

    // ========================================
}
