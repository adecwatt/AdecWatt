package adecWatt.view;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPopupMenu;

import misc.Util;

import adecWatt.control.AdecWattManager;
import adecWatt.model.Item;
import adecWatt.model.AdecWatt;
import adecWatt.model.User;

@SuppressWarnings ("serial") public class JItemPopup extends JPopupMenu {

    private Item getLocal (JWorkspaceView<?> jWorkspaceView, Item item) {
	return jWorkspaceView.workspace.getCloseEmbedded (item);
    }
    private List<Item> getLocal2 (JWorkspaceView<?> jWorkspaceView, Collection<Item> selectedItems) {
	ArrayList<Item> localSelectedItems = new ArrayList<Item> (selectedItems.size ());
	for (Item item : selectedItems)
	    localSelectedItems.add (jWorkspaceView.workspace.getCloseEmbedded (item));
	return localSelectedItems;
    }

    // ========================================
    public JItemPopup (final JWorkspaceView<?> jWorkspaceView, final Item item, final Point pos) {
	final AdecWatt adecWatt = jWorkspaceView.getAdecWatt ();
	Item localItem = jWorkspaceView.workspace.getLocalEmbedded (item.getId ()); // XXX a supprimer
	User user = adecWatt.getUser ();
	boolean editor = user.isEditor (jWorkspaceView.getWorkspace ());

	Util.addMenuItem (AdecWattManager.actionDisplayItem, new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    adecWatt.broadcastDisplay (AdecWattManager.actionDisplayItem, jWorkspaceView.getSelectedItems ());
		}
	    }, this);
	if (editor) {
	    Util.addMenuItem (AdecWattManager.actionModifyItem, new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			adecWatt.broadcastDisplay (AdecWattManager.actionModifyItem, getLocal2 (jWorkspaceView, jWorkspaceView.getSelectedItems ()));
		    }
		}, this);
	    Util.addMenuItem (AdecWattManager.actionHideItem, new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			adecWatt.broadcastDisplay (AdecWattManager.actionHideShowItem, jWorkspaceView, jWorkspaceView.getSelectedItems (), true);
		    }
		}, this);
	    Util.addMenuItem (AdecWattManager.actionShowItem, new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			adecWatt.broadcastDisplay (AdecWattManager.actionHideShowItem, jWorkspaceView, jWorkspaceView.getSelectedItems (), false);
		    }
		}, this);
	    Util.addMenuItem (AdecWattManager.actionTransformItem, new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			adecWatt.broadcastDisplay (AdecWattManager.actionTransformItem, getLocal (jWorkspaceView, item));
		    }
		}, this);
	    Util.addMenuItem (AdecWattManager.actionCopyItem, new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			adecWatt.broadcastDisplay (AdecWattManager.actionCopyItem, jWorkspaceView, jWorkspaceView.getSelectedItems (), pos);
		    }
		}, this);
	    // XXX a mettre dans le fond
	    if (localItem != null)
		Util.addMenuItem (AdecWattManager.actionRemoveItem, new ActionListener () {
			public void actionPerformed (ActionEvent e) {
			    adecWatt.broadcastDisplay (AdecWattManager.actionRemoveItem, jWorkspaceView, jWorkspaceView.getSelectedItems ());
			}
		    }, this);
	}
	show (jWorkspaceView, pos.x, pos.y);
    }

    // ========================================
}
