package adecWatt.view;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import adecWatt.model.Item;
import adecWatt.control.AdecWattManager;

@SuppressWarnings ("serial") public class JOverlapItemPopup extends JPopupMenu {
    JWorkspaceView<?> jWorkspaceView;
    MouseEvent mouseEvent;
    
    public JOverlapItemPopup (JWorkspaceView<?> jWorkspaceView, MouseEvent mouseEvent) {
	this.jWorkspaceView = jWorkspaceView;
	this.mouseEvent = mouseEvent;
	Point pos = mouseEvent.getPoint ();
	if (jWorkspaceView.selectedAcc != null) {
	    Point2D.Double realPos = jWorkspaceView.scaleViewToModel (new Point (pos.x, pos.y));
	    if (jWorkspaceView.selectedAcc.contains (realPos, jWorkspaceView.closePixelsAccs/jWorkspaceView.scale))
	    	return;
	}
	ArrayList<Item> overlapItems = jWorkspaceView.findItems (pos);
	if (mouseEvent.getID () == MouseEvent.MOUSE_PRESSED &&
	    !mouseEvent.isShiftDown () &&
	    !SwingUtilities.isRightMouseButton (mouseEvent) &&
	    jWorkspaceView.selectedItems.size () > 0) {
	    for (final JItem jItem : jWorkspaceView.selectedItems.values ())
		if (overlapItems.contains (jItem.getItem ())) {
		    jWorkspaceView.startDrag (jItem.getItem (), mouseEvent.getPoint ());
		    return;
		}
	}
	switch (overlapItems.size ()) {
	case 0:
	    select (null);
	    return;
	case 1:
	    select (overlapItems.get (0));
	    return;
	}
	if (SwingUtilities.isRightMouseButton (mouseEvent) && jWorkspaceView.selectedItems.size () > 0) {
	    if (jWorkspaceView.performPopup (mouseEvent))
		return;
	}
	for (final Item item : overlapItems) { 
	    JMenuItem menuItem = new JMenuItem (item.getName (), item.getParentUnit ().getIcon (16));
	    menuItem.setActionCommand (item.getName ());
	    menuItem.addActionListener (new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			select (item);
		    }
		});
	    add (menuItem);
	}
	jWorkspaceView.noDrag ();
	show (jWorkspaceView, pos.x, pos.y);
    }

    public void select (Item item) {
	switch (mouseEvent.getID ()) {
	case MouseEvent.MOUSE_PRESSED:
	    if (SwingUtilities.isRightMouseButton (mouseEvent)) {
		if (jWorkspaceView.performPopup (mouseEvent))
		    return;
		jWorkspaceView.performSelection (item, mouseEvent);
		jWorkspaceView.performPopup (mouseEvent);
		return;
	    }
	    jWorkspaceView.performSelection (item, mouseEvent);
	    break;
	case MouseEvent.MOUSE_CLICKED:
	    if (mouseEvent.getClickCount () > 1) {
		// double click
		if (jWorkspaceView.selectedItems.size () < 1)
		    jWorkspaceView.setSelectedItems (item);
		ArrayList<Item> param = new ArrayList<Item> ();
		for (final JItem jItem : jWorkspaceView.selectedItems.values ())
		    param.add (jItem.getItem ());
		jWorkspaceView.adecWatt.broadcastDisplay (AdecWattManager.actionDisplayItem, param);
		return;
	    }
	    break;
	}
    }
}
