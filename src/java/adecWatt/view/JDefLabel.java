package adecWatt.view;

import java.util.List;
import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings ("serial")
public class JDefLabel extends JPanel {

    private Vector<String> refList = new Vector<String> ();
    private Vector<String> list = new Vector<String> ();
    private JTextField[] labels;

    public List<String> getLabels () {
	if (refList.size () < 1)
	    return null;
	return refList;
    }

    public JDefLabel (List<String> multiLabel) {
	if (multiLabel == null)
	    return;
	refList = new Vector<String> (multiLabel);
    }
    public void initLabels (int nbLabel) {
	removeAll ();
	labels = new JTextField[nbLabel];
	for (int i = 0; i < nbLabel; i++) {
	    String ref = refList != null && refList.size () > i ? refList.get (i) : null;
	    add (labels[i] = new JTextField (ref, 8));
	}
    }
    public void confirmLabels () {
	refList.clear ();
	if (labels == null)
	    return;
	for (JTextField label : labels)
	    refList.add (label.getText ());
    }
}
