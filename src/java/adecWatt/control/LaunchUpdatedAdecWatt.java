package adecWatt.control;

import java.io.File;

import misc.Config;
import misc.RemoteUpdate;

public class LaunchUpdatedAdecWatt {

    // ========================================
    static public void main (String[] args) {
	Config.setPWD (LaunchUpdatedAdecWatt.class);
	File dir = Config.getPWD ().getParentFile ();
	RemoteUpdate.renameNewFile (dir);

	File softDir = new File (dir, "soft");
	File jar = new File (softDir, "AdecWatt.jar");
	RemoteUpdate.launch (jar);
    }

    // ========================================
}
