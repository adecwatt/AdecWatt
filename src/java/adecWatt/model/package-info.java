/**
 * Architecture des classes
 *
 * AdecWatt = appli
 * PermanentDB
 * UnitNode
 * 
 * Permanent
 *   Prop (property)
 *   Editable : prop[] (Container)
 *     Embedded
 *       Item (contient des accessoires)
 *         Segm : information avec 2 extrémintés
 *         Comp : NonWorkspace (container d'Acc)
 *       Acc : Comp
 *     Unit
 *       NonWorkspace (container d'Acc)
 *         Accessory
 *         Furniture
 *         Line
 *         Information
 *       Workspace : comp[] (container de comp)
 *         Building
 *         Lightplot : Building
 * 
 */
package adecWatt.model;
