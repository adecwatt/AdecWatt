package adecWatt.model;

import java.awt.Color;
import java.lang.reflect.Field;
import java.util.Hashtable;

public class LPColor {

    static public Hashtable<String, LeeFilter> leeFilterList = new Hashtable<String, LeeFilter> ();

    static public class LeeFilter {
	String ref;
	int value;
	String comment;

	public LeeFilter (String ref, int value, String comment) {
	    this.ref = ref;
	    this.value = value;
	    this.comment = comment;
	    leeFilterList.put ("LF"+ref, this);
	}
    }

    static public Color getColor (String color) {
	if (color == null || color.isEmpty ())
	    return Color.gray;
	LeeFilter leeFilter = leeFilterList.get (color.replaceAll ("\\s", "").toUpperCase ());
	if (leeFilter != null)
	    return new Color (leeFilter.value);
	color = color.toLowerCase ();
	try {
	    Field field = Class.forName ("java.awt.Color").getField (color);
	    return (Color) field.get (null);
	} catch (Exception e) {
	}
	try {
	    return new Color (Integer.valueOf (color.substring (0, 2), 16),
			      Integer.valueOf (color.substring (2, 4), 16),
			      Integer.valueOf (color.substring (4, 6), 16));
	} catch (Exception e) {
	}
	return Color.black;
    }

    static {
	new LeeFilter ("002", 0xFF78DC, "Rose Pink");
	new LeeFilter ("003", 0xFAF0FA, "Lavender Tint");
	new LeeFilter ("004", 0xFFC8B4, "Medium Bastard Amber");
	new LeeFilter ("007", 0xFAFAD2, "Pale Yellow");
	new LeeFilter ("008", 0xFC9B80, "Dark Salmon");
	new LeeFilter ("009", 0xFCD7B3, "Pale Amber Gold");
	new LeeFilter ("010", 0xFFFF00, "Medium Yellow");
	new LeeFilter ("013", 0xFCD89B, "Straw Tint");
	new LeeFilter ("015", 0xFFCD00, "Deep Straw");
	new LeeFilter ("017", 0xE68C64, "Surprise Peach");
	new LeeFilter ("019", 0xFF4600, "Fire");
	new LeeFilter ("020", 0xFFBE55, "Medium Amber");
	new LeeFilter ("021", 0xFF8C32, "Gold Amber");
	new LeeFilter ("022", 0xFF6900, "Dark Amber");
	new LeeFilter ("024", 0xFF5A64, "Scarlet");
	new LeeFilter ("025", 0xFF6E46, "Sunset Red");
	new LeeFilter ("026", 0xE6003C, "Bright Red");
	new LeeFilter ("027", 0xC8003C, "Medium Red");
	new LeeFilter ("029", 0xE1003C, "Plasa Red");
	new LeeFilter ("035", 0xFFC8D2, "Light Pink");
	new LeeFilter ("036", 0xFFA0B9, "Medium Pink");
	new LeeFilter ("046", 0xDC005A, "Dark Magenta");
	new LeeFilter ("048", 0xE66EAF, "Rose Purple");
	new LeeFilter ("049", 0xBE0091, "Medium Purple");
	new LeeFilter ("052", 0xE6AAFA, "Light Lavender");
	new LeeFilter ("053", 0xE6E6FA, "Paler Lavender");
	new LeeFilter ("058", 0xB46EF0, "Lavender");
	new LeeFilter ("061", 0xDCF5FF, "Mist Blue");
	new LeeFilter ("063", 0xD2F5FF, "Pale Blue");
	new LeeFilter ("068", 0x46B4F0, "Sky Blue");
	new LeeFilter ("071", 0x0000B4, "Tokyo Blue");
	new LeeFilter ("075", 0x64A0F0, "Evening Blue");
	new LeeFilter ("079", 0x3C8CD2, "Just Blue");
	new LeeFilter ("085", 0x006EB9, "Deeper Blue");
	new LeeFilter ("088", 0xDCFF64, "Lime Green");
	new LeeFilter ("089", 0x5ADC5A, "Moss Green");
	new LeeFilter ("090", 0x00BE00, "Dark Yellow Green");
	new LeeFilter ("100", 0xF5FF00, "Spring Yellow");
	new LeeFilter ("101", 0xFFF500, "Yellow");
	new LeeFilter ("102", 0xFFDC5F, "Light Amber");
	new LeeFilter ("103", 0xFCEACC, "Straw");
	new LeeFilter ("104", 0xFFDC00, "Deep Amber");
	new LeeFilter ("105", 0xFFA000, "Orange");
	new LeeFilter ("106", 0xF00032, "Primary Red");
	new LeeFilter ("107", 0xFFA5AF, "Light Rose");
	new LeeFilter ("108", 0xFCBEA9, "English Rose");
	new LeeFilter ("109", 0xFFB2AF, "Light Salmon");
	new LeeFilter ("110", 0xFFB4C8, "Middle Rose");
	new LeeFilter ("111", 0xFF8CBE, "Dark Pink");
	new LeeFilter ("113", 0xFF0064, "Magenta");
	new LeeFilter ("115", 0x00EBC8, "Peacock Blue");
	new LeeFilter ("116", 0x00C8B9, "Medium Blue-Green");
	new LeeFilter ("117", 0xB4FAF5, "Steel Blue");
	new LeeFilter ("118", 0x00E1EB, "Light Blue");
	new LeeFilter ("119", 0x0078C8, "Dark Blue");
	new LeeFilter ("120", 0x005FBE, "Deep Blue");
	new LeeFilter ("121", 0xB4FF64, "LEE Green");
	new LeeFilter ("122", 0x78FA6E, "Fern Green");
	new LeeFilter ("124", 0x00DC78, "Dark Green");
	new LeeFilter ("126", 0xBE009B, "Mauve");
	new LeeFilter ("127", 0xE16273, "Smokey Pink");
	new LeeFilter ("128", 0xFF50B4, "Bright Pink");
	new LeeFilter ("129", 0xFFFFFF, "Heavy Frost");
	new LeeFilter ("130", 0xFFFFFF, "Clear");
	new LeeFilter ("131", 0x64FAD2, "Marine Blue");
	new LeeFilter ("132", 0x00A0DC, "Medium Blue");
	new LeeFilter ("134", 0xFAA873, "Golden Amber");
	new LeeFilter ("135", 0xFF5F00, "Deep Golden Amber");
	new LeeFilter ("136", 0xF0BEE6, "Pale Lavender");
	new LeeFilter ("137", 0xC8B4E6, "Special Lavender");
	new LeeFilter ("138", 0xDCFFA0, "Pale Green");
	new LeeFilter ("139", 0x4BC300, "Primary Green");
	new LeeFilter ("140", 0x87F0EB, "Summer Blue");
	new LeeFilter ("141", 0x00D2E6, "Bright Blue");
	new LeeFilter ("142", 0xAAAAF0, "Pale Violet");
	new LeeFilter ("143", 0x64BED2, "Pale Navy Blue");
	new LeeFilter ("144", 0x5AE1E6, "No Colour Blue");
	new LeeFilter ("147", 0xFCB98C, "Apricot");
	new LeeFilter ("148", 0xFF507D, "Bright Rose");
	new LeeFilter ("151", 0xFFCDC3, "Gold Tint");
	new LeeFilter ("152", 0xFFD2C1, "Pale Gold");
	new LeeFilter ("153", 0xFFC8C8, "Pale Salmon");
	new LeeFilter ("154", 0xFFD5CF, "Pale Rose");
	new LeeFilter ("156", 0xE1B48C, "Chocolate");
	new LeeFilter ("157", 0xFF92A3, "Pink");
	new LeeFilter ("158", 0xFF8700, "Deep Orange");
	new LeeFilter ("159", 0xFFFAEB, "No Colour Straw");
	new LeeFilter ("161", 0x7DD2F5, "Slate Blue");
	new LeeFilter ("162", 0xFCDED8, "Bastard Amber");
	new LeeFilter ("164", 0xFF3200, "Flame Red");
	new LeeFilter ("165", 0x5AC8EB, "Daylight Blue");
	new LeeFilter ("169", 0xFADCF0, "Lilac Tint");
	new LeeFilter ("170", 0xE6AADC, "Deep Lavender");
	new LeeFilter ("172", 0x00DCDC, "Lagoon Blue");
	new LeeFilter ("174", 0xAFE1FA, "Dark Steel Blue");
	new LeeFilter ("176", 0xFFAAA0, "Loving Amber");
	new LeeFilter ("179", 0xFFBE00, "Chrome Orange");
	new LeeFilter ("180", 0xA064E6, "Dark Lavender");
	new LeeFilter ("181", 0x5000AA, "Congo Blue");
	new LeeFilter ("182", 0xF50000, "Light Red");
	new LeeFilter ("183", 0x00D7E3, "Moonlight Blue");
	new LeeFilter ("184", 0xF0E1D7, "Cosmetic Peach");
	new LeeFilter ("186", 0xF8DCDD, "Cosmetic Silver Rose");
	new LeeFilter ("187", 0xF5E4D7, "Cosmetic Rouge");
	new LeeFilter ("188", 0xFFEBD7, "Cosmetic Highlight");
	new LeeFilter ("189", 0xEEEDD8, "Cosmetic Silver Moss");
	new LeeFilter ("191", 0xE9EEEE, "Cosmetic Aqua Blue");
	new LeeFilter ("192", 0xFF8CB4, "Flesh Pink");
	new LeeFilter ("194", 0xBE8CF0, "Surprise Pink");
	new LeeFilter ("195", 0x006ED2, "Zenith Blue");
	new LeeFilter ("196", 0x78D2E6, "True Blue");
	new LeeFilter ("197", 0x82AAE6, "Alice Blue");
	new LeeFilter ("198", 0x6478C8, "Palace Blue");
	new LeeFilter ("199", 0x6464E6, "Regal Blue");
	new LeeFilter ("200", 0x91BEF5, "Double C.T. Blue");
	new LeeFilter ("201", 0xC3E1FA, "Full C.T. Blue");
	new LeeFilter ("202", 0xD7F0FF, "Half C.T. Blue");
	new LeeFilter ("203", 0xEBFCFF, "Quarter C.T. Blue");
	new LeeFilter ("204", 0xFAC387, "Full C.T. Orange");
	new LeeFilter ("205", 0xFCD9B1, "Half C.T. Orange");
	new LeeFilter ("206", 0xFCEAD6, "Quarter C.T. Orange");
	new LeeFilter ("207", 0xF0B46B, "Full C.T. Orange + .3 ND");
	new LeeFilter ("208", 0xE6A550, "Full C.T. Orange + .6 ND");
	new LeeFilter ("209", 0xC8C8C8, "0.3 ND");
	new LeeFilter ("210", 0xA5A5A5, "0.6 ND");
	new LeeFilter ("211", 0x828282, "0.9 ND");
	new LeeFilter ("212", 0xFFFAD7, "L.C.T.Yellow (Y1)");
	new LeeFilter ("213", 0xE6FCDC, "White Flame Green");
	new LeeFilter ("214", 0xFFFFFF, "Full Tough Spun");
	new LeeFilter ("215", 0xFFFFFF, "Half Tough Spun");
	new LeeFilter ("216", 0xFFFFFF, "White Diffusion");
	new LeeFilter ("217", 0xF5FFFF, "Blue Diffusion");
	new LeeFilter ("218", 0xF5FFFF, "Eighth C.T. Blue");
	new LeeFilter ("219", 0x9BDCAF, "LEE Fluorescent Green");
	new LeeFilter ("220", 0xFFFFFF, "White Frost");
	new LeeFilter ("221", 0xF5FFFF, "Blue Frost");
	new LeeFilter ("223", 0xFFF3E8, "Eighth C.T. Orange");
	new LeeFilter ("224", 0x96A0BE, "Daylight Blue Frost");
	new LeeFilter ("225", 0x9EA0A2, "Neutral Density Frost");
	new LeeFilter ("226", 0xFFFFFA, "LEE U.V.");
	new LeeFilter ("228", 0xFFFFFF, "Brushed Silk");
	new LeeFilter ("229", 0xFFFFFF, "Quarter Tough Spun");
	new LeeFilter ("230", 0xDCBE8C, "Super Correction L.C.T. Yellow");
	new LeeFilter ("232", 0xEDBE83, "Super Correction W.F. Green");
	new LeeFilter ("236", 0xFAC795, "H.M.I. (to Tungsten)");
	new LeeFilter ("237", 0xFCB292, "C.I.D. (to Tungsten)");
	new LeeFilter ("238", 0xF5B9AA, "C.S.I. (to Tungsten)");
	new LeeFilter ("239", 0xCDCDCD, "Polariser");
	new LeeFilter ("241", 0xAADCC3, "LEE Fluorescent 5700 Kelvin");
	new LeeFilter ("242", 0xB9E6B9, "LEE Fluorescent 4300 Kelvin");
	new LeeFilter ("243", 0xCDF5AF, "LEE Fluorescent 3600 Kelvin");
	new LeeFilter ("244", 0xE6FABE, "LEE Plus Green");
	new LeeFilter ("245", 0xF0FCD2, "Half Plus Green");
	new LeeFilter ("246", 0xF5FFE6, "Quarter Plus Green");
	new LeeFilter ("247", 0xFAC3D7, "LEE Minus Green");
	new LeeFilter ("248", 0xFFE2E4, "Half Minus Green");
	new LeeFilter ("249", 0xFFECF0, "Quarter Minus Green");
	new LeeFilter ("250", 0xFFFFFF, "Half White Diffusion");
	new LeeFilter ("251", 0xFFFFFF, "Quarter White Diffusion");
	new LeeFilter ("252", 0xFFFFFF, "Eighth White Diffusion");
	new LeeFilter ("253", 0xFFFFFF, "Hampshire Frost");
	new LeeFilter ("254", 0xFFFFFF, "New Hampshire Frost");
	new LeeFilter ("255", 0xFFFFFF, "Hollywood Frost");
	new LeeFilter ("256", 0xFFFFFF, "Half Hampshire Frost");
	new LeeFilter ("257", 0xFFFFFF, "Quarter Hampshire Frost");
	new LeeFilter ("258", 0xFFFFFF, "Eighth Hampshire Frost");
	new LeeFilter ("261", 0xFFFFFF, "Tough Spun FR - Full");
	new LeeFilter ("262", 0xFFFFFF, "Tough Spun FR - 3/4");
	new LeeFilter ("263", 0xFFFFFF, "Tough Spun FR - 1/2");
	new LeeFilter ("264", 0xFFFFFF, "Tough Spun FR - 3/8");
	new LeeFilter ("265", 0xFFFFFF, "Tough Spun FR - 1/4");
	new LeeFilter ("269", 0xFFFFFF, "LEE Heat Shield");
	new LeeFilter ("270", 0xBEBEBE, "LEE Scrim");
	new LeeFilter ("271", 0xF0F0F0, "Mirror Silver");
	new LeeFilter ("272", 0xF0DC96, "Soft Gold Reflector");
	new LeeFilter ("273", 0xF3F3F3, "Soft Silver Reflector");
	new LeeFilter ("274", 0xEBD791, "Mirror Gold");
	new LeeFilter ("275", 0xBEBEBE, "Black Scrim");
	new LeeFilter ("278", 0xF3FFF3, "Eighth Plus Green");
	new LeeFilter ("279", 0xFFF3F7, "Eighth Minus Green");
	new LeeFilter ("280", 0x000000, "Black Foil");
	new LeeFilter ("281", 0xCDE6FA, "Three Quarter C.T. Blue");
	new LeeFilter ("283", 0xAFD9F5, "One and a Half C.T. Blue");
	new LeeFilter ("285", 0xFCCD94, "Three Quarter C.T. Orange");
	new LeeFilter ("286", 0xFFB464, "One and a Half C.T. Orange");
	new LeeFilter ("287", 0xFFA055, "Double C.T. Orange");
	new LeeFilter ("298", 0xE6E6E6, "0.15 ND");
	new LeeFilter ("299", 0x5F5F5F, "1.2 ND");
	new LeeFilter ("322", 0x41F5BE, "Soft Green");
	new LeeFilter ("323", 0x00E1AA, "Jade");
	new LeeFilter ("327", 0x008C50, "Forest Green");
	new LeeFilter ("328", 0xFF64C8, "Follies Pink");
	new LeeFilter ("332", 0xFF3787, "Special Rose Pink");
	new LeeFilter ("343", 0x8C00D2, "Special Medium Lavender");
	new LeeFilter ("345", 0xCD6ED7, "Fuchsia Pink");
	new LeeFilter ("352", 0x5AC8E1, "Glacier Blue");
	new LeeFilter ("353", 0x61E8E3, "Lighter Blue");
	new LeeFilter ("354", 0x00F0D7, "Special Steel Blue");
	new LeeFilter ("363", 0x006EC3, "Special Medium Blue");
	new LeeFilter ("366", 0xAAD2F0, "Cornflower");
	new LeeFilter ("400", 0xFFFFFF, "LEELux");
	new LeeFilter ("402", 0xFFFFFF, "Soft Frost");
	new LeeFilter ("404", 0xFFFFFF, "Half Soft Frost");
	new LeeFilter ("410", 0xFFFFFF, "Opal Frost");
	new LeeFilter ("414", 0xFFFFFF, "Highlight");
	new LeeFilter ("416", 0xFFFFFF, "Three Quarter White Diffusion");
	new LeeFilter ("420", 0xFFFFFF, "Light Opal Frost");
	new LeeFilter ("429", 0xFFFFFF, "Quiet Frost");
	new LeeFilter ("430", 0xFFFFFF, "Grid Cloth");
	new LeeFilter ("432", 0xFFFFFF, "Light Grid Cloth");
	new LeeFilter ("434", 0xFFFFFF, "Quarter Grid Cloth");
	new LeeFilter ("439", 0xFFFFFF, "Heavy Quiet Frost");
	new LeeFilter ("441", 0xFAC084, "Full C.T. Straw");
	new LeeFilter ("442", 0xFCDCAD, "Half C.T. Straw");
	new LeeFilter ("443", 0xFCEFDB, "Quarter C.T. Straw");
	new LeeFilter ("444", 0xFAF3E8, "Eighth C.T. Straw");
	new LeeFilter ("450", 0xFFFFFF, "Three Eighth White Diffusion");
	new LeeFilter ("452", 0xFFFFFF, "Sixteenth White Diffusion");
	new LeeFilter ("460", 0xFFFFFF, "Quiet Grid Cloth");
	new LeeFilter ("462", 0xFFFFFF, "Quiet Light Grid Cloth");
	new LeeFilter ("464", 0xFFFFFF, "Quiet Quarter Grid Cloth");
	new LeeFilter ("500", 0xB4D2FF, "Double New Colour Blue");
	new LeeFilter ("501", 0xD7EBFA, "New Colour Blue (Robertson Blue)");
	new LeeFilter ("502", 0xE1F0FF, "Half New Colour Blue");
	new LeeFilter ("503", 0xF4FAFF, "Quarter New Colour Blue");
	new LeeFilter ("504", 0xD2F5DC, "Waterfront Green");
	new LeeFilter ("505", 0xE3FF5A, "Sally Green");
	new LeeFilter ("506", 0xFFDACE, "Marlene");
	new LeeFilter ("507", 0xF85000, "Madge");
	new LeeFilter ("508", 0x5A46C3, "Midnight Maya");
	new LeeFilter ("511", 0x963C00, "Bacon Brown");
	new LeeFilter ("512", 0xcc6100, "Amber Delight");
	new LeeFilter ("513", 0xf5ff7d, "Ice and a Slice");
	new LeeFilter ("514", 0xf5ff5a, "Double G&T");
	new LeeFilter ("525", 0x8EB4FA, "Argent Blue");
	new LeeFilter ("550", 0xe3ca3c, "ALD Gold");
	new LeeFilter ("600", 0x98a8aa, "Arctic White");
	new LeeFilter ("601", 0x90a0a0, "Silver");
	new LeeFilter ("602", 0xa5a5aa, "Platinum");
	new LeeFilter ("603", 0xc8cdcd, "Moonlight White");
	new LeeFilter ("604", 0xFFC295, "Full C.T. Eight Five");

	new LeeFilter ("642", 0x968200, "Half Mustard Yellow");
	new LeeFilter ("643", 0xB4A000, "Quarter Mustard Yellow");
	new LeeFilter ("650", 0xC8A862, "Industry Sodium");
	new LeeFilter ("651", 0xFF9B5F, "HI Sodium");
	new LeeFilter ("652", 0xFF8246, "Urban Sodium");
	new LeeFilter ("653", 0x965500, "LO Sodium");
	new LeeFilter ("700", 0x7D00CD, "Perfect Lavender");
	new LeeFilter ("701", 0xB45ADC, "Provence");
	new LeeFilter ("702", 0xE6D2F0, "Special Pale Lavender");
	new LeeFilter ("703", 0xD282DC, "Cold Lavender");
	new LeeFilter ("704", 0xF0AAFA, "Lily");
	new LeeFilter ("705", 0xF0AAFA, "Lily Frost");
	new LeeFilter ("706", 0x6E46C8, "King Fals Lavender");
	new LeeFilter ("707", 0x6400B4, "Ultimate Violet");
	new LeeFilter ("708", 0xDCEBFA, "Cool Lavender");
	new LeeFilter ("709", 0xC8C8FA, "Electric Lilac");
	new LeeFilter ("710", 0x8CA0F0, "Spir Special Blue");
	new LeeFilter ("711", 0xAABEDC, "Cold Blue");
	new LeeFilter ("712", 0xA0BEF0, "Bedford Blue");
	new LeeFilter ("713", 0x003CA0, "J.Winter Blue");
	new LeeFilter ("714", 0x5A91D2, "Elysian Blue");
	new LeeFilter ("715", 0x3C6EDC, "Cabana Blue");
	new LeeFilter ("716", 0x0064D2, "Mikkel Blue");
	new LeeFilter ("717", 0xC3E1FA, "Shanklin Frost");
	new LeeFilter ("718", 0xD4EDFA, "Half Shanklin Frost");
	new LeeFilter ("719", 0x9BC3F0, "Colour Wash Blue");
	new LeeFilter ("720", 0xC3E1FA, "Durham Daylight Frost");
	new LeeFilter ("721", 0x0082E6, "Berry Blue");
	new LeeFilter ("722", 0x008CD2, "Bray Blue");
	new LeeFilter ("723", 0x5082DC, "Virgin Blue");
	new LeeFilter ("724", 0x69E1EB, "Ocean Blue");
	new LeeFilter ("725", 0xBEF2F3, "Old Steel Blue");
	new LeeFilter ("727", 0x00A5B4, "QFD Blue");
	new LeeFilter ("728", 0xC8EBD2, "Steel Green");
	new LeeFilter ("729", 0x00AFAA, "Scuba Blue");
	new LeeFilter ("730", 0xDCFFE6, "Liberty Green");
	new LeeFilter ("731", 0xE1FAD7, "Dirty Ice");
	new LeeFilter ("733", 0xEBF7CF, "Damp Squib");
	new LeeFilter ("735", 0x00BE78, "Velvet Green");
	new LeeFilter ("736", 0x00AA00, "Twickenham Green");
	new LeeFilter ("738", 0xAAFF00, "JAS Green");
	new LeeFilter ("740", 0x5A6E00, "Aurora Borealis Green");
	new LeeFilter ("741", 0x826400, "Mustard Yellow");
	new LeeFilter ("742", 0xE19B50, "Bram Brown");
	new LeeFilter ("744", 0xF8C882, "Dirty White");
	new LeeFilter ("746", 0x6E3C00, "Brown");
	new LeeFilter ("747", 0xF5B99F, "Easy White");
	new LeeFilter ("748", 0xE96785, "Seedy Pink");
	new LeeFilter ("749", 0xFCD3CD, "Hampshire Rose");
	new LeeFilter ("750", 0xFAF1FE, "Durham Frost");
	new LeeFilter ("763", 0xFCF0D2, "Wheat");
	new LeeFilter ("764", 0xFCE6B4, "Sun Colour Straw");
	new LeeFilter ("765", 0xFFE591, "LEE Yellow");
	new LeeFilter ("767", 0xFFE600, "Oklahoma Yellow");
	new LeeFilter ("768", 0xFFC600, "Egg Yolk Yellow");
	new LeeFilter ("770", 0xFFB400, "Burnt Yellow");
	new LeeFilter ("773", 0xFCC5B2, "Cardbox Amber");
	new LeeFilter ("774", 0xFCD2BE, "Soft Amber Key 1");
	new LeeFilter ("775", 0xFCC6A5, "Soft Amber Key 2");
	new LeeFilter ("776", 0xFCBE9B, "Nectarine");
	new LeeFilter ("777", 0xF58200, "Rust");
	new LeeFilter ("778", 0xFF7600, "Millennium Gold");
	new LeeFilter ("779", 0xFC9885, "Bastard Pink");
	new LeeFilter ("780", 0xFF6F00, "AS Golden Amber");
	new LeeFilter ("781", 0xFF5000, "Terry Red");
	new LeeFilter ("787", 0xB9003C, "Marius Red");
	new LeeFilter ("789", 0xAA3C32, "Blood Red");
	new LeeFilter ("790", 0xFFB4A5, "Moroccan Pink");
	new LeeFilter ("791", 0xF5B9AA, "Moroccan Frost");
	new LeeFilter ("793", 0xFF3CA0, "Vanity Fair");
	new LeeFilter ("794", 0xFFAFDC, "Pretty 'n Pink");
	new LeeFilter ("795", 0xFA46C8, "Magical Magenta");
	new LeeFilter ("797", 0xAF0096, "Deep Purple");
	new LeeFilter ("798", 0xA000BE, "Chrysalis Pink");
	new LeeFilter ("799", 0x3C00B4, "Special KH Lavender");

	new LeeFilter ("CL104", 0xff9600, "Cool LED Deep Amber");
	new LeeFilter ("CL105", 0xff6e00, "Cool LED Orange");
	new LeeFilter ("CL106", 0xbe0032, "Cool LED Primary Red");
	new LeeFilter ("CL113", 0xc80050, "Cool LED Magenta");
	new LeeFilter ("CL115", 0x00ffa0, "Cool LED Peacock Blue");
	new LeeFilter ("CL116", 0x00aa82, "Cool LED Medium Blue-Green");
	new LeeFilter ("CL117", 0xebffa0, "Cool LED Steel Blue");
	new LeeFilter ("CL118", 0x00f5b4, "Cool LED Light Blue");
	new LeeFilter ("CL119", 0x0091a5, "Cool LED Dark Blue");
	new LeeFilter ("CL126", 0xaf0050, "Cool LED Mauve");
	new LeeFilter ("CL128", 0xff0069, "Cool LED Bright Pink");
	new LeeFilter ("CL132", 0x00c8c8, "Cool LED Medium Blue");
	new LeeFilter ("CL139", 0x4bc300, "Cool LED Primary Green");
	new LeeFilter ("CL147", 0xff7d46, "Cool LED Apricot");
	new LeeFilter ("CL158", 0xff6900, "Cool LED Deep Orange");
	new LeeFilter ("CL164", 0xff0000, "Cool LED Flame Red");
	new LeeFilter ("CL180", 0xeb78a0, "Cool LED Dark Lavender");
	new LeeFilter ("CL181", 0x960078, "Cool LED Congo Blue");
	new LeeFilter ("CL182", 0xdc0052, "Cool LED Light Red");

	new LeeFilter ("414P", 0xFFFFFF, "Perforated Highlight");
	new LeeFilter ("439P", 0xFFFFFF, "Perforated Heavy Quiet Frost");
	new LeeFilter ("A205", 0xFAD7AF, "Half C.T.O. Acrylic Panel");
	new LeeFilter ("A207", 0xF3B76E, "Full C.T.O. + 0.3 ND Acrylic Panel");
	new LeeFilter ("A208", 0xE7A651, "Full C.T.O. + 0.6 ND Acrylic Panel");
	new LeeFilter ("A209", 0xCDCDCD, "0.3 ND Acrylic Panel");
	new LeeFilter ("A210", 0xAAAAAA, "0.6 ND Acrylic Panel");
	new LeeFilter ("A211", 0x878787, "0.9 ND Acrylic Panel");
    }

    // <option value="zircon.html#zircon-minus-green" data-colbit="#FACDD7">801: Zircon Minus Green 1</option>
    // <option value="zircon.html#zircon-minus-green" data-colbit="#FAE7EA">802: Zircon Minus Green 2</option>
    // <option value="zircon.html#zircon-minus-green" data-colbit="#FCF2FA">803: Zircon Minus Green 3</option>
    // <option value="zircon.html#zircon-minus-green" data-colbit="#FCF7FC">804: Zircon Minus Green 4</option>
    // <option value="zircon.html#zircon-minus-green" data-colbit="#FCFAFC">805: Zircon Minus Green 5</option>
    // <option value="zircon.html#zircon-warm-amber" data-colbit="#FFAA6E">806: Zircon Warm Amber 2</option>
    // <option value="zircon.html#zircon-warm-amber" data-colbit="#FFD291">807: Zircon Warm Amber 4</option>
    // <option value="zircon.html#zircon-warm-amber" data-colbit="#FFE4AA">808: Zircon Warm Amber 6</option>
    // <option value="zircon.html#zircon-warm-amber" data-colbit="#FFEBB4">809: Zircon Warm Amber 8</option>
    // <option value="zircon.html#zircon-diffusion" data-colbit="#FFFFFF">810: Zircon Diffusion 1</option>
    // <option value="zircon.html#zircon-diffusion" data-colbit="#FFFFFF">811: Zircon Diffusion 2</option>
    // <option value="zircon.html#zircon-diffusion" data-colbit="#FFFFFF">812: Zircon Diffusion 3</option>
}
