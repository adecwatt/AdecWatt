package adecWatt.model.xml;

import java.util.ArrayList;
import org.w3c.dom.Node;

import adecWatt.model.Permanent;
import adecWatt.model.Unit;

/**
 */
public class XmlUnit extends XmlPermanent<Permanent.UnitTypeEnum, Permanent.UnitAttrEnum> {

    // ========================================
    //public Unit unit;

    public ArrayList<XmlComp> comps = new ArrayList<XmlComp> ();
    public ArrayList<XmlProp> attributes = new ArrayList<XmlProp> ();

    // ========================================

    public XmlUnit (Node xmlUnitNode) {
	parseNode (xmlUnitNode, Unit.UnitTypeEnum.class, Unit.UnitAttrEnum.class);
    }

    // ========================================
}
