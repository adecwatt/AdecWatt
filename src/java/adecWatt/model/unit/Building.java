package adecWatt.model.unit;

import java.util.Enumeration;

import adecWatt.model.AdecWatt;
import adecWatt.model.UnitNode;

public class Building extends Workspace {

    // ========================================
    static public final String[] workspacesRoot = { "lightplot" };

    public Building (AdecWatt adecWatt, String id) {
	super (adecWatt, id);
    }

    // ========================================
    public void updateView () {
	super.updateView ();
	for (String workspaceRoot : workspacesRoot)
	    for (Enumeration<?> enumUnitNode = adecWatt.getPermanentDB ().getRootByName (workspaceRoot).getUnitNode ().breadthFirstEnumeration ();
		 enumUnitNode.hasMoreElements ();
		 )
		adecWatt.broadcastDisplay (AdecWatt.BroadcastUpdateWorkspace, (Lightplot) ((UnitNode<?>) enumUnitNode.nextElement ()).getUnit ());
    }

    // ========================================
}
