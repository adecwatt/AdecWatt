package adecWatt.model;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import misc.Util;

public class PreviewCache {
    static {
 	ImageIO.setUseCache (false);
    }

    static public final String[] imageExtentions = { "gif", "jpg", "jpeg", "png", "tif", "tiff", "bmp" };
    static public final List<String> imageExtentionsList = Arrays.asList (imageExtentions);
    static public final FileFilter fileImageFilter = new FileFilter () {
	    public boolean accept (File file) {
		try {
		    return file.isFile () && !file.isHidden () &&
			imageExtentionsList.contains (Util.getExtention (file).toLowerCase ());
		} catch (Exception e) {
		    return false;
		}
	    }
	};

    static public int thumbSide = 40; // XXX
    static public Dimension thumbDim = new Dimension (thumbSide, thumbSide); // XXX
    static public final long maxSize = 1024*1024; // XXX int MAX_FILE_PREVIEW

    private File dir;

    public PreviewCache (File dir) {
	this.dir = dir;
    }

    private Hashtable<File, Boolean> toCleanList = new Hashtable<File, Boolean> ();
    public synchronized void prepareClean () {
	try {
	    toCleanList.clear ();
	    for (File file : dir.listFiles (fileImageFilter))
		toCleanList.put (file, true);
	} catch (Exception e) {
	}
    }
    public synchronized void clean () {
	for (File file : toCleanList.keySet ())
	    if (toCleanList.get (file))
		file.delete ();
	toCleanList.clear ();
    }
    public synchronized void clean (String imageName) {
	(new File (dir, Util.getBase (imageName)+".png")).delete (); // XXX YYY
	
    }
    public synchronized ImageIcon getIcon (File file) {
	if (file == null || (maxSize > 0 && file.length () > maxSize))
	    return null;
	File cacheFile = new File (dir, Util.getBase (file)+".png"); // XXX YYY
	if (cacheFile.exists () && cacheFile.lastModified () > file.lastModified ()) {
	    toCleanList.put (cacheFile, false);
	    return new ImageIcon (cacheFile.getPath ());
	}
	try {
	    BufferedImage bufferedImage = ImageIO.read (file);
	    if (bufferedImage == null)
		return null;
	    int width = bufferedImage.getWidth ();
	    int height = bufferedImage.getHeight ();
	    if (width == 0 || height == 0)
		return null;
	    Image image = null;
	    if (width > height) {
		if (width > thumbSide) {
		    height = Math.max (1, (height*thumbSide)/width);
		    width = thumbSide;
		    image = bufferedImage.getScaledInstance (width, height, Image.SCALE_DEFAULT);
		}
	    } else {
		if (height > thumbSide) {
		    width = Math.max (1, (width*thumbSide)/height);
		    height = thumbSide;
		    image = bufferedImage.getScaledInstance (width, height, Image.SCALE_DEFAULT);
		}
	    }
	    if (image != null) {
		bufferedImage = new BufferedImage (width, height, BufferedImage.TYPE_INT_ARGB);
		bufferedImage.createGraphics ().drawImage (image, 0, 0, null);
	    }
	    try {
		dir.mkdirs ();
		ImageIO.write (bufferedImage, "png", cacheFile);
		toCleanList.put (cacheFile, false);
	    } catch (Exception e) {
		e.printStackTrace ();
	    }
	    return new ImageIcon (bufferedImage);
	} catch (Exception e) {
	    e.printStackTrace ();
	    return null;
	}
    }

}
